# pi-cluster

## Infastructure

2 Raspberry Pi 4's
1 Synology NAS

## Base Software

RPi's running Ubuntu Server 20.04

Installed K3s - https://k3s.io/

## K3s Deployments

### Applications

[Gitea](https://gitea.com)

### Utilities

[NFS Subdir External Provisioner](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner)

## Notes

### Domain Used

fener.net

DNSMasq set up on router to point fener.net to master node local ip

If using a different domain, update values files

### Enabling Traefik dashboard

Edit the traefik manifest on the master node

`sudo vi /var/lib/rancher/k3s/server/manifests/traefik.yaml`

Under `spec.valuesContent` add `dashbord.enabled: true` and `dashboard.domaint: traefik.fenert.net`

### Setting default storage class

`kubectl patch storageclass local-path -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'`